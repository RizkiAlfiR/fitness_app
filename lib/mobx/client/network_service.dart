import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:fitnessapp/mobx/models/exercises_resp.dart';

class NetworkService {
  List<ExercisesResp> exerciseResp = List();

  Future getExercise(String url) async {
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      exerciseResp = (data['exercises'] as List).map((json) {
        return ExercisesResp.fromJson(json);
      }).toList();
      return exerciseResp;
    } else {
      print("Error in URL");
    }
  }

//  var response = await http.get(API_URL);
//  var body = response.body;
//
//  var decodeJson = jsonDecode(body);
//
//  exerciseHub = ExerciseHub.fromJson(decodeJson);
}
