import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:fitnessapp/mobx/models/exercises_resp.dart';
import 'package:fitnessapp/mobx/stores/exercises_store.dart';
import 'package:fitnessapp/ui/exerciseDetail_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ExercisesStore _exercisesStore = ExercisesStore();

  ExercisesList() {
    _exercisesStore.getDataExercises();
  }

  @override
  Widget build(BuildContext context) {
    final future = _exercisesStore.exercisesListFuture;

    return Scaffold(
      appBar: AppBar(
        title: Text("Fitness App"),
      ),
      body: Observer(
        builder: (_) {
          switch (future.status) {
            case FutureStatus.pending:
              return LinearProgressIndicator();

            case FutureStatus.rejected:
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Failed to load items.',
                      style: TextStyle(color: Colors.red),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RaisedButton(
                      child: const Text('Tap to retry'),
                      onPressed: _refresh,
                    )
                  ],
                ),
              );

            case FutureStatus.fulfilled:
              final List<ExercisesResp> exercises = future.result;
              print(exercises);

//              return RefreshIndicator(
//                onRefresh: _refresh,
//                child: ListView.builder(
//                    physics: const AlwaysScrollableScrollPhysics(),
//                    itemCount: exercises.length,
//                    itemBuilder: (context, index) {
//                      final exercise = exercises[index];
//                      return InkWell(
//                        onTap: () {
////                        Navigator.push(
////                            context,
////                            MaterialPageRoute(
////                              builder: (context) => ExcerciseDetailPage(
////                                exercises: e,
////                              ),
////                            ));
//                        },
//                        child: Hero(
////                        tag: e.id,
//                          child: Container(
//                            margin: EdgeInsets.all(10),
//                            decoration: BoxDecoration(
//                                borderRadius: BorderRadius.circular(16)),
//                            child: Stack(
//                              children: <Widget>[
//                                ClipRRect(
//                                  borderRadius: BorderRadius.circular(16),
//                                  child: Stack(
//                                    children: <Widget>[
//                                      CachedNetworkImage(
//                                        imageUrl: exercise.exercises
//                                            .map((e) => e.thumbnail),
//                                        placeholder: (context, url) => Image(
//                                          image: AssetImage(
//                                              "assets/placeholder.jpg"),
//                                          width:
//                                              MediaQuery.of(context).size.width,
//                                          height: 250,
//                                          fit: BoxFit.cover,
//                                        ),
//                                        errorWidget: (context, url, error) =>
//                                            Icon(Icons.error),
//                                        fit: BoxFit.cover,
//                                        width:
//                                            MediaQuery.of(context).size.width,
//                                        height: 250,
//                                      ),
//                                    ],
//                                  ),
//                                ),
//                                ClipRRect(
//                                  borderRadius: BorderRadius.circular(16),
//                                  child: Container(
//                                    width: MediaQuery.of(context).size.width,
//                                    height: 250,
//                                    decoration: BoxDecoration(
//                                      gradient: LinearGradient(
//                                        colors: [
//                                          Color(0xFF000000),
//                                          Color(0x00000000),
//                                        ],
//                                        begin: Alignment.bottomCenter,
//                                        end: Alignment.center,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                                Container(
//                                  height: 250,
//                                  padding: EdgeInsets.only(
//                                    left: 10,
//                                    bottom: 10,
//                                  ),
//                                  alignment: Alignment.bottomLeft,
//                                  child: Text(
//                                    e.title,
//                                    style: TextStyle(
//                                      fontSize: 18,
//                                      color: Colors.white,
//                                    ),
//                                  ),
//                                )
//                              ],
//                            ),
//                          ),
//                        ),
//                      );
//                    }
////                  children: exerciseHub.exercises.map((e) {
////                    return InkWell(
////                      onTap: () {
////                        Navigator.push(
////                            context,
////                            MaterialPageRoute(
////                              builder: (context) => ExcerciseDetailPage(
////                                exercises: e,
////                              ),
////                            ));
////                      },
////                      child: Hero(
////                        tag: e.id,
////                        child: Container(
////                          margin: EdgeInsets.all(10),
////                          decoration: BoxDecoration(
////                              borderRadius: BorderRadius.circular(16)),
////                          child: Stack(
////                            children: <Widget>[
////                              ClipRRect(
////                                borderRadius: BorderRadius.circular(16),
////                                child: Stack(
////                                  children: <Widget>[
////                                    CachedNetworkImage(
////                                      imageUrl: e.thumbnail,
////                                      placeholder: (context, url) => Image(
////                                        image: AssetImage(
////                                            "assets/placeholder.jpg"),
////                                        width:
////                                            MediaQuery.of(context).size.width,
////                                        height: 250,
////                                        fit: BoxFit.cover,
////                                      ),
////                                      errorWidget: (context, url, error) =>
////                                          Icon(Icons.error),
////                                      fit: BoxFit.cover,
////                                      width: MediaQuery.of(context).size.width,
////                                      height: 250,
////                                    ),
////                                  ],
////                                ),
////                              ),
////                              ClipRRect(
////                                borderRadius: BorderRadius.circular(16),
////                                child: Container(
////                                  width: MediaQuery.of(context).size.width,
////                                  height: 250,
////                                  decoration: BoxDecoration(
////                                    gradient: LinearGradient(
////                                      colors: [
////                                        Color(0xFF000000),
////                                        Color(0x00000000),
////                                      ],
////                                      begin: Alignment.bottomCenter,
////                                      end: Alignment.center,
////                                    ),
////                                  ),
////                                ),
////                              ),
////                              Container(
////                                height: 250,
////                                padding: EdgeInsets.only(
////                                  left: 10,
////                                  bottom: 10,
////                                ),
////                                alignment: Alignment.bottomLeft,
////                                child: Text(
////                                  e.title,
////                                  style: TextStyle(
////                                    fontSize: 18,
////                                    color: Colors.white,
////                                  ),
////                                ),
////                              )
////                            ],
////                          ),
////                        ),
////                      ),
////                    );
////                  }).toList(),
//                    ),
//              );
              break;
          }
        },
      ),
    );
  }

//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text("Fitness App"),
//      ),
//      body: Container(
//        child: exerciseHub != null
//            ?
//            GridView.count(
//                crossAxisCount: 2,
//                children: exerciseHub.exercises.map((e) {
//                  return InkWell(
//                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                            builder: (context) => ExcerciseDetailPage(
//                              exercises: e,
//                            ),
//                          ));
//                    },
//                    child: Hero(
//                      tag: e.id,
//                      child: Container(
//                        margin: EdgeInsets.all(10),
//                        decoration: BoxDecoration(
//                            borderRadius: BorderRadius.circular(16)),
//                        child: Stack(
//                          children: <Widget>[
//                            ClipRRect(
//                              borderRadius: BorderRadius.circular(16),
////                              child: FadeInImage(
////                                placeholder:
////                                    AssetImage("assets/placeholder.jpg"),
////                                image: NetworkImage(e.thumbnail),
////                                width: MediaQuery.of(context).size.width,
////                                height: 250,
////                                fit: BoxFit.cover,
////                              ),
//                              child: Stack(
//                                children: <Widget>[
//                                  CachedNetworkImage(
//                                    imageUrl: e.thumbnail,
//                                    placeholder: (context, url) => Image(
//                                      image:
//                                          AssetImage("assets/placeholder.jpg"),
//                                      width: MediaQuery.of(context).size.width,
//                                      height: 250,
//                                      fit: BoxFit.cover,
//                                    ),
//                                    errorWidget: (context, url, error) =>
//                                        Icon(Icons.error),
//                                    fit: BoxFit.cover,
//                                    width: MediaQuery.of(context).size.width,
//                                    height: 250,
//                                  ),
//                                ],
//                              ),
//                            ),
//                            ClipRRect(
//                              borderRadius: BorderRadius.circular(16),
//                              child: Container(
//                                width: MediaQuery.of(context).size.width,
//                                height: 250,
//                                decoration: BoxDecoration(
//                                  gradient: LinearGradient(
//                                    colors: [
//                                      Color(0xFF000000),
//                                      Color(0x00000000),
//                                    ],
//                                    begin: Alignment.bottomCenter,
//                                    end: Alignment.center,
//                                  ),
//                                ),
//                              ),
//                            ),
//                            Container(
//                              height: 250,
//                              padding: EdgeInsets.only(
//                                left: 10,
//                                bottom: 10,
//                              ),
//                              alignment: Alignment.bottomLeft,
//                              child: Text(
//                                e.title,
//                                style: TextStyle(
//                                  fontSize: 18,
//                                  color: Colors.white,
//                                ),
//                              ),
//                            )
//                          ],
//                        ),
//                      ),
//                    ),
//                  );
//                }).toList(),
//              )
//            : LinearProgressIndicator(),
//      ),
//    );
//  }
  Future _refresh() => _exercisesStore.fetchExercises();
}
