import 'package:mobx/mobx.dart';
import 'package:fitnessapp/mobx/client/network_service.dart';
import 'package:fitnessapp/mobx/models/exercises_resp.dart';

//part 'exercise_store.g.dart';
//
//class ExercisesStore = _ExercisesStore with $ExercisesStore;

class ExercisesStore with Store {
  final NetworkService httpClient = NetworkService();

  @observable
  ObservableFuture<List<ExercisesResp>> exercisesListFuture;

  @action
  Future fetchExercises() => exercisesListFuture = ObservableFuture(httpClient
      .getExercise(
          'https://raw.githubusercontent.com/codeifitech/fitness-app/master/exercises.json')
      .then((exercises) => exercises));

  void getDataExercises() {
    fetchExercises();
  }
}
